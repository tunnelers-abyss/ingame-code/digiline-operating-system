--display the entered command with the classic greater-than-smybol
if(event.channel=="input")then digiline_send("lcd",">"..event.msg) end

--Initialise all variables before starting!
if (event.channel=="reset" and event.msg=="reset")then
  mem.files={}  
  mem.name={}
  mem.i=1 
  mem.command=true
  digiline_send("lcd","System reseted")
  mem.dir=mem.files
end

--WRITE MODE
if(event.channel=="input" and mem.write and not mem.command)then
mem.dir[mem.target]=mem.dir[mem.target]..event.msg 
mem.write=false
mem.command=true
digiline_send("lcd", "file "..mem.target.." saved")
end

--COMMAND MODE
--List files
if((event.msg=="ls" or event.msg=="ls -l" or event.msg=="list")and mem.command) then  
  mem.j=1
  while mem.j<=#mem.name do
    if(mem.dir[mem.name[mem.j]]~=nil)then
      digiline_send("lcd", mem.name[mem.j])
    end
    mem.j=mem.j+1
  end

--Read files
elseif(event.channel=="input" and string.sub(event.msg,1,4)=="read" and mem.command)then
  mem.target=string.sub(event.msg,6)
  digiline_send("lcd", tostring(mem.dir[mem.target]))


--Write Files
elseif(event.channel=="input" and string.sub(event.msg,1,5)=="write" and mem.command)then
  --Check if file already exists.
  mem.target=string.sub(event.msg,7)
  if(mem.dir[mem.target]==nil)then
    digiline_send("lcd","Enter your text")
    mem.write=true  
    mem.command=false
    mem.name[mem.i]=mem.target
    mem.dir[mem.target]=""
    mem.i=mem.i+1
  else
    if (tostring(type(mem.dir[mem.target]))=="string")then
      digiline_send("lcd","Continue on\n"..mem.target)
      digiline_send("lcd","Enter your text")
      mem.write=true  
      mem.command=false
    else
      digiline_send("lcd",mem.target.." not a text")
    end
  end

--Delete files
elseif(event.channel=="input" and string.sub(event.msg,1,6)=="delete" and mem.command)then
  mem.target=string.sub(event.msg,8)
  mem.dir[mem.target]=nil
  digiline_send("lcd","File deleted")  

--make directory
elseif(event.channel=="input" and string.sub(event.msg,1,5)=="mkdir" and mem.command)then
  --Check if file already exists.
  mem.target=string.sub(event.msg,7)
  if tostring(mem.dir[mem.target])=="nil" then
    mem.name[mem.i]=mem.target
    mem.i=mem.i+1
    mem.dir[mem.target]={}
    digiline_send("lcd","Made directory"..mem.target)  
  else
    digiline_send("lcd","already exists")  
  end



--cd
elseif(event.channel=="input" and string.sub(event.msg,1,2)=="cd" and mem.command)then
  mem.line=tostring(event.msg)
  if(#mem.line<=3)then
    --go to root directory
    mem.dir=mem.files
    digiline_send("lcd","Go to root")
  else
    mem.target=string.sub(event.msg,4)
    if(tostring(type(mem.dir[mem.target]))=="table")then
      mem.dir=mem.dir[mem.target]
      digiline_send("lcd","Go to dirctory")
    else
      digiline_send("lcd","Not found")
    end
     
  end


--screensaver
elseif(event.channel=="input" and event.msg=="screensaver" and mem.command)then
digiline_send("lcd","______ _\n| ___ \\ | Operating\n| |_/ / |_ System__\n| ___ \\ | | ||/ _ \\\n| |_/ / | |_||  __/\n\\____/|_|\\__,|\\___|")  

 --help
    elseif(event.channel == "input" and event.msg == "help" and mem.command) then
        digiline_send("lcd","Commands:\nlist, screensaver\nread\nwrite\ndelete\nturnon\nturnoff\ntime\ndate\nsetch\ndsend")
    --Screensave
    elseif(event.channel == "input" and event.msg == "screensaver" and mem.command) then
        digiline_send("lcd", "______ _\n| ___ \\ | Operating\n| |_/ / |_ System__\n| ___ \\ | | ||/ _ \\\n| |_/ / | |_||  __/\n\\____/|_|\\__,|\\___|")  

    --Mesecon-Interace
    elseif(event.channel == "input" and string.sub(event.msg,1,6) == "turnon" and mem.command ) then 
      -- Turn on ports
        if (string.sub(event.msg, 8) == 'a') then
            port.a = true
        elseif (string.sub(event.msg, 8) == 'b') then
            port.b = true
        elseif (string.sub(event.msg, 8) == 'c') then
            port.c = true
        elseif (string.sub(event.msg, 8) == 'd') then
            port.d = true   
        else
            digiline_send("lcd" ,"Invalid port")
        end
    
    elseif(event.channel == "input" and string.sub(event.msg, 1, 7) == "turnoff" and mem.command ) then
      -- Turn off ports
        if (string.sub(event.msg, 9) =='a') then
        port.a = false
        elseif (string.sub(event.msg, 9) == 'b') then
            port.b = flase
        elseif (string.sub(event.msg, 9) == 'c') then
            port.c = false
        elseif (string.sub(event.msg, 9) == 'd') then
            port.d = false 
        else
            digiline_send("lcd" ,"Invalid port")
        end
    
    elseif( event.channel == "input" and string.sub(event.msg, 1, 4) == "time" and mem.command) then
        -- Show time
        time = os.datetable()
        digiline_send("lcd",time.hour..":"..time.min..":"..time.sec.." UTC")
    
    elseif( event.channel == "input" and string.sub(event.msg, 1, 4) == "date" and mem.command) then
        -- show date
        date = os.datetable()
        digiline_send("lcd",date.day.."/"..date.month.."/"..date.year)


    --Communication Protocol    
    elseif ( event.channel == "input" and string.sub(event.msg, 1, 5) == "dsend" and mem.command) then
        mem.dsend = true
        mem.command = false
        mem.dsendchannel = string.sub(event.msg, 7)
        digiline_send("lcd","Enter the message to be sent>")
    elseif(event.channel == "input" and string.sub(event.msg,1,5)=="setch" and mem.command) then
        mem.dsendch = string.sub(event.msg,7)
        digiline_send("lcd", "channel set to "..mem.dsendch)
    elseif (event.channel == mem.dsendch ) then
        digiline_send("lcd","message from connected computer: "..event.msg)
        
    else
     --   digiline_send("lcd" ,"Unknown Command")
    end
    
