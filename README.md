# Digiline Operating System

A Git-lab repository  of Code by Antares From Aurelium server.

# Running the code on a local machine:
**This is Just for testing for errors only. The code do not work as expected.**
- install lua
- Initialize the debug.lua first
```
$lua
Lua 5.4.1  Copyright (C) 1994-2020 Lua.org, PUC-Rio
> dofile("debug.lua")
> event_init()
Enter Event type
digiline
enter channel
input
enter message
list
> dofile("init.lua")
Channel:        lcd
Message:        >list
```
# Running the code on Minetest:
## Required mods:

- Digiline.
- Mesecons.
- Digiline Terminal.
## Circuit:
- place lua controller.
- place digiline terminal near the lua controller.
- copy and paste the code on lua controller.
- Then reset the board to initialze the command by setting the digiline send channel as "reset" and receive channel as "lcd" in digiline Terminal. Send "reset" on input field
- after that set channel send  channel to "input" and start typing commands in the input field of digiline terminal.
# Commands available
- ls/list : list files
- read <filename> : read the content of file
- write <filename> : write/append to file
- setch <channelname> : set the digiline channel for communication
- dsend <channelname> : send text to digiline channel. (hit enter. after that type the content to be sent) 
    usage example:
    ```
    dsend a
    Enter the message to be sent>
    hi
    
    ```
- time : shows time (UTC)
- date : shows date (UTC)
- help : display list of commands
- turnon <port> : turns on the port on lua controller (port can be a/b/c/d)
- turnoff <port> : turs off a port on lua controller  (port can be a/b/c/d)
